
# Description To Code VSCode Extension

This repo holds the code for the VSCode Description2Code editor extension.


## Plan

The extension will allow users to hit cmd+Enter to send their line of text to the server.
Then the server will return the top results for the generated Python code which the user will be able to jump between and correct.
The users corrections will then be sent back to the model and used to improve it.
