'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as request from "request-promise-native";

async function getCode(description: string, sourceCode: string, currentLine: number) {
    var options = {
        method: 'POST',
        uri: 'http://127.0.0.1:4444/get_code',
        json: true,
        body: {
            description: description,
            source_code: sourceCode,
            line_number: currentLine
        }
    };
    const response = await request.post(options);
    if ('error' in response) {
        console.log(response);
        // vscode.window.showErrorMessage(response['message']);
        vscode.window.showInputBox(
            {placeHolder: 'Error: ' + response['message']}
        );
        return null;
    }
    if ('code_options' in response) {
        return {
            'predictionId': response['prediction_id'],
            'code_options': response['code_options']
        };
    }
    return {
        'predictionId': response['prediction_id'],
        'code': response['code']
    };
}

function writeCode(code: string) {
    let editor = vscode.window.activeTextEditor;
    if (typeof editor === 'undefined') {
        return null;
    }
    const edit = new vscode.WorkspaceEdit();
    const res = [
        vscode.TextEdit.insert(editor.selection.active, code)
    ];
    edit.set(editor.document.uri, res);
    vscode.workspace.applyEdit(edit);
}

async function saveUserResponse(prediction_id: string, confirmed_code: string, description: string) {
    var options = {
        method: 'POST',
        uri: 'http://127.0.0.1:4444/save_prediction_response',
        json: true,
        body: {
            code: confirmed_code,
            prediction_id: prediction_id,
            prompt: 'code for "' + description + '"'
        }
    };
    const response = await request.post(options);
    if ('error' in response) {
        console.log(response);
        return null;
    } else {
        console.log('Saved response');
    }
}

function newCodeEditInput(description: string, code: string) {
    if (code !== '') {
        return vscode.window.showInputBox(
            {
                value: code,
                placeHolder: 'Enter correct code.',
                prompt: 'code for "' + description + '"'
            }
        );
    }
    return vscode.window.showInputBox(
        {
            placeHolder: 'Model unsure. Enter correct code for "' + description + '".',
            prompt: 'code for "' + description + '"'
        }
    );
}

function editCode(predictionId: string, description: string, code: string) {
    let codeEditInput = newCodeEditInput(description, code);

    codeEditInput.then((possibleEdit) => {
        if (typeof possibleEdit === 'undefined') {
            return null;
        }
        const codeEdit = String(possibleEdit);

        if (codeEdit !== "") {
            writeCode(codeEdit);
            saveUserResponse(predictionId, codeEdit, description);
        }
    });
}

async function saveObject(data: object) {
    var options = {
        method: 'POST',
        uri: 'http://127.0.0.1:4444/save_object',
        json: true,
        body: {'data': allContentChanges}
    };
    const response = await request.post(options);
    console.log(response);
}

let allContentChanges: any[] = [];

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    vscode.workspace.onDidChangeTextDocument((e) => {
        console.log(e.contentChanges);
        // need to save these con
        allContentChanges.push({
            content: e.contentChanges,
            time: new Date()
        });
    });

    let saveLogs = vscode.commands.registerCommand('extension.saveLogs', () => {
        saveObject(allContentChanges);
    });
    context.subscriptions.push(saveLogs);

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.newDescription', () => {
        // The code you place here will be executed every time your command is executed

        let descriptionInput = vscode.window.showInputBox(
            {placeHolder: 'your description', prompt: 'Describe the code you want.'}
        );
        descriptionInput.then((possibleDescription) => {
            if (typeof possibleDescription === 'undefined') {
                return null;
            }
            const description: string = String(possibleDescription);
            var editor = vscode.window.activeTextEditor;
            if (!editor) {
                return null; // No open text editor
            }
            const sourceCode: string = editor.document.getText();
            const currentLine: number = editor.selection.active.line;

            let codeRequest = getCode(description, sourceCode, currentLine);
            codeRequest.then((possiblePrediction) => {
                if (typeof possiblePrediction === 'undefined' || possiblePrediction === null) {
                    return null;
                }
                const predictionId: string = String(possiblePrediction['predictionId']);
                let code: string = "";
                if ('code_options' in possiblePrediction) {
                    const code_options: string[] = possiblePrediction['code_options'];

                    let codePick = vscode.window.showQuickPick(code_options, {placeHolder: 'Select one.'});
                    codePick.then((possibleCode) => {
                        if (typeof possibleCode === 'undefined') {
                            return null;
                        }
                        code = String(possibleCode);
                        editCode(predictionId, description, code);
                    });
                } else {
                    code = String(possiblePrediction['code']);
                    editCode(predictionId, description, code);
                }
            });
            codeRequest.catch((reason) => {
                console.log(reason);
            });

        });

    });

    context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
}